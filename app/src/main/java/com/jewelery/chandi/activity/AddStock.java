package com.jewelery.chandi.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.jewelery.chandi.R;
import com.jewelery.chandi.model.StockModel;
import com.jewelery.chandi.model.TransactionModel;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddStock extends AppCompatActivity {
    @BindView(R.id.pattiStock)
    TextView pattiStock;
    @BindView(R.id.articleStock)
    TextView articleStock;
    @BindView(R.id.pureStock)
    TextView pureStock;
    @BindView(R.id.patti)
    RadioButton patti;
    @BindView(R.id.article)
    RadioButton article;
    @BindView(R.id.pure)
    RadioButton pure;
    @BindView(R.id.type)
    RadioGroup type;
    @BindView(R.id.weight)
    EditText weight;
    @BindView(R.id.weightPanel)
    LinearLayout weightPanel;
    @BindView(R.id.addStock)
    Button addStock;
    @BindView(R.id.download)
    Button download;
    @BindView(R.id.clear)
    Button clear;
    private DatabaseReference mDatabase;
    private ValueEventListener valueEventListener;
    private DatabaseReference mDatabase1;
    private ValueEventListener valueEventListener1;
    StockModel stockModel = new StockModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_stock);
        ButterKnife.bind(this);

        dbConnect();

        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadTransactions();
            }
        });

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlert(AddStock.this);
            }
        });
    }

    private void showAlert(Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Do you want to reset all stocks to 0?")
            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    resetStock();
                }
            })
            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // do nothing
                    dialog.dismiss();
                }
            })
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show();
    }

    private void resetStock() {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("stock");
        mDatabase.child("pattiStock").setValue("0");
        mDatabase.child("articleStock").setValue("0");
        mDatabase.child("pureStock").setValue("0");
    }

    private void downloadTransactions() {
        showProgress();
        mDatabase1 = FirebaseDatabase.getInstance().getReference("stock").child("transactions");
        valueEventListener1 = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                hideProgress();
                ArrayList<TransactionModel> models = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    TransactionModel model = snapshot.getValue(TransactionModel.class);
                    models.add(model);
                }
                if (models.size() == 0) {
                    Toast.makeText(AddStock.this, "No transactions found!", Toast.LENGTH_SHORT).show();
                    return;
                }
                exportToPDF(models);
                mDatabase1.removeEventListener(valueEventListener1);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        };
        mDatabase1.addValueEventListener(valueEventListener1);
    }

    private void exportToPDF(ArrayList<TransactionModel> models) {
        try {
            //Create file path for Pdf
            File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString());
            if (!folder.exists()) {
                Log.e("tag", "not exist");
                folder.mkdir();
            } else {
                Log.e("tag", "exist");
            }
            String name = "stock-" + new SimpleDateFormat("dd-MM-yyyy").format(System.currentTimeMillis());
            File file = new File(folder, name + ".pdf");
            // To customise the text of the pdf
            // we can use FontFamily
            Font font1 = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD, new BaseColor(0, 0, 0));
            //Font bf12 = new Font(Font.FontFamily.TIMES_ROMAN, 12);
            // create an instance of itext document
            Document document = new Document(PageSize.A4, 20, 20, 20, 20);
            PdfWriter.getInstance(document, new FileOutputStream(file.getAbsoluteFile()));
            document.open();
            document.addTitle("Stock Report");
            document.addSubject("Stock Report");

            // container of all views
            Paragraph preface = new Paragraph();

            // table
            PdfPTable table = new PdfPTable(4);
            table.setWidthPercentage(100);
            // setting table header
            PdfPCell c1 = new PdfPCell(new Phrase("Date Added"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
            c1.setPadding(3.0f);
            table.addCell(c1);
            PdfPCell c2 = new PdfPCell(new Phrase("Patti"));
            c2.setHorizontalAlignment(Element.ALIGN_CENTER);
            c2.setVerticalAlignment(Element.ALIGN_MIDDLE);
            c2.setPadding(3.0f);
            table.addCell(c2);
            PdfPCell c3 = new PdfPCell(new Phrase("Article"));
            c3.setHorizontalAlignment(Element.ALIGN_CENTER);
            c3.setVerticalAlignment(Element.ALIGN_MIDDLE);
            c3.setPadding(3.0f);
            table.addCell(c3);
            PdfPCell c4 = new PdfPCell(new Phrase("Pure"));
            c4.setHorizontalAlignment(Element.ALIGN_CENTER);
            c4.setVerticalAlignment(Element.ALIGN_MIDDLE);
            c4.setPadding(3.0f);
            table.addCell(c4);

            table.setHeaderRows(1);
            // setting table values
            Collections.reverse(models);
            ArrayList<TransactionModel> newModels = new ArrayList<>();
            for (int i = 0; i < models.size(); i++) {
                if (newModels.size() < 30) {
                    newModels.add(models.get(i));
                } else {
                    break;
                }
            }
            Collections.reverse(newModels);
            for (TransactionModel model : newModels) {
                PdfPCell one = new PdfPCell(new Phrase(model.getDate()));
                one.setHorizontalAlignment(Element.ALIGN_CENTER);
                one.setVerticalAlignment(Element.ALIGN_MIDDLE);
                one.setPadding(3.0f);
                table.addCell(one);

                PdfPCell two = new PdfPCell(new Phrase(model.getPatti() + " gm"));
                two.setHorizontalAlignment(Element.ALIGN_CENTER);
                two.setVerticalAlignment(Element.ALIGN_MIDDLE);
                two.setPadding(3.0f);
                table.addCell(two);

                PdfPCell three = new PdfPCell(new Phrase(model.getArticle() + " gm"));
                three.setHorizontalAlignment(Element.ALIGN_CENTER);
                three.setVerticalAlignment(Element.ALIGN_MIDDLE);
                three.setPadding(3.0f);
                table.addCell(three);

                PdfPCell four = new PdfPCell(new Phrase(model.getPure() + " gm"));
                four.setHorizontalAlignment(Element.ALIGN_CENTER);
                four.setVerticalAlignment(Element.ALIGN_MIDDLE);
                four.setPadding(3.0f);
                table.addCell(four);
            }

            preface.add(table);
            document.add(preface);
            // close document
            document.close();
            Toast.makeText(this, "Document Downloaded!", Toast.LENGTH_SHORT).show();
            openPDF(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openPDF(File file) {
        Uri uri1 = Uri.fromFile(file);
        Uri uri2 = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".my.package.name.provider", file);
        Log.e("tag", "uri1: " + uri1);
        Log.e("tag", "uri2: " + uri2);
        try {
            Intent intentUrl = new Intent(Intent.ACTION_VIEW);
            if (Build.VERSION.SDK_INT < 24) {
                intentUrl.setDataAndType(uri1, "application/pdf");
            } else {
                intentUrl.setDataAndType(uri2, "application/pdf");
                intentUrl.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }
            intentUrl.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intentUrl);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "No PDF Viewer Installed", Toast.LENGTH_LONG).show();
        }
    }

    private void dbConnect() {
        showProgress();
        mDatabase = FirebaseDatabase.getInstance().getReference("stock");
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                hideProgress();
                if (dataSnapshot == null)
                    return;
                stockModel = dataSnapshot.getValue(StockModel.class);
                if (stockModel == null)
                    stockModel = new StockModel();
                pattiStock.setText("Patti Stock:    " + stockModel.getPattiStock() + " gm");
                articleStock.setText("Article Stock:    " + stockModel.getArticleStock() + " gm");
                pureStock.setText("Pure Stock:    " + stockModel.getPureStock() + " gm");
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        };
        mDatabase.addValueEventListener(valueEventListener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mDatabase != null) {
            mDatabase.removeEventListener(valueEventListener);
        }
    }

    @OnClick(R.id.addStock)
    public void onViewClicked() {
        if (weight.getText().toString().isEmpty()) {
            weight.setError("Required");
            return;
        }
        if (weight.getText().toString().equals("0")) {
            weight.setError("Should be greater than 0");
            return;
        }

        String oldStock = "";
        String addedStock = "";
        String newStock = "";
        String stockType = "";
        if (type.getCheckedRadioButtonId() == R.id.patti) {
            double total = Double.parseDouble(stockModel.getPattiStock()) + Double.parseDouble(weight.getText().toString());
            oldStock = stockModel.getPattiStock();
            addedStock = weight.getText().toString();
            newStock = String.format("%.3f", total);
            stockType = "Patti";
            mDatabase.child("pattiStock").setValue(String.format("%.3f", total));
            TransactionModel model = new TransactionModel();
            model.setDate(new SimpleDateFormat("dd-MM-yyyy").format(System.currentTimeMillis()));
            model.setPatti(addedStock);
            model.setArticle("0");
            model.setPure("0");
            mDatabase.child("transactions").push().setValue(model);

        } else if (type.getCheckedRadioButtonId() == R.id.article) {
            double total = Double.parseDouble(stockModel.getArticleStock()) + Double.parseDouble(weight.getText().toString());
            oldStock = stockModel.getArticleStock();
            addedStock = weight.getText().toString();
            newStock = String.format("%.3f", total);
            stockType = "Article";
            mDatabase.child("articleStock").setValue(String.format("%.3f", total));
            TransactionModel model = new TransactionModel();
            model.setDate(new SimpleDateFormat("dd-MM-yyyy").format(System.currentTimeMillis()));
            model.setPatti("0");
            model.setArticle(addedStock);
            model.setPure("0");
            mDatabase.child("transactions").push().setValue(model);
        } else if (type.getCheckedRadioButtonId() == R.id.pure) {
            double total = Double.parseDouble(stockModel.getPureStock()) + Double.parseDouble(weight.getText().toString());
            oldStock = stockModel.getPureStock();
            addedStock = weight.getText().toString();
            newStock = String.format("%.3f", total);
            stockType = "Pure";
            mDatabase.child("pureStock").setValue(String.format("%.3f", total));
            TransactionModel model = new TransactionModel();
            model.setDate(new SimpleDateFormat("dd-MM-yyyy").format(System.currentTimeMillis()));
            model.setPatti("0");
            model.setArticle("0");
            model.setPure(addedStock);
            mDatabase.child("transactions").push().setValue(model);
        }
        showCustomDialog(oldStock, addedStock, newStock, stockType);
    }

    public void showCustomDialog(String oldStock_, String addedStock_, String newStock_, String stockType_) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog);

        TextView oldStock = (TextView) dialog.findViewById(R.id.oldStock);
        TextView addedStock = (TextView) dialog.findViewById(R.id.addedStock);
        TextView newStock = (TextView) dialog.findViewById(R.id.newStock);
        oldStock.setText("Old " + stockType_ + " Stock: " + oldStock_ + " gm");
        addedStock.setText("Added Stock: " + addedStock_ + " gm");
        newStock.setText("New " + stockType_ + " Stock: " + newStock_ + " gm");

        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    ProgressDialog progressDialog;

    public void showProgress() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
    }

    public void hideProgress() {
        if (progressDialog != null)
            progressDialog.hide();
    }

}
