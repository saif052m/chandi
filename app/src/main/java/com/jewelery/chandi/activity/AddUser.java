package com.jewelery.chandi.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jewelery.chandi.R;
import com.jewelery.chandi.model.StockModel;
import com.jewelery.chandi.model.UserModel;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddUser extends AppCompatActivity {
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.addUser)
    Button addUser;
    @BindView(R.id.viewUsers)
    Button viewUsers;
    private DatabaseReference mDatabase;
    private ValueEventListener valueEventListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.addUser, R.id.viewUsers})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.addUser:
                addUser();
                break;
            case R.id.viewUsers:
                Intent intent = new Intent(this, ViewUsers.class);
                startActivity(intent);
                break;
        }
    }

    private void addUser() {
        if(email.getText().toString().isEmpty()){
            email.setError("Required");
            return;
        }
        if(password.getText().toString().isEmpty()){
            password.setError("Required");
            return;
        }
        showProgress();
        mDatabase = FirebaseDatabase.getInstance().getReference("user");
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                hideProgress();
                if(dataSnapshot.exists()){
                    //Toast.makeText(AddUser.this, "Already exists!", Toast.LENGTH_SHORT).show();
                }
                else{
                    UserModel userModel = new UserModel();
                    Random r = new Random();
                    userModel.setId(""+(r.nextInt(1000)+r.nextInt(1000)+r.nextInt(1000)+r.nextInt(1000)));
                    userModel.setEmail(email.getText().toString());
                    userModel.setPassword(password.getText().toString());
                    mDatabase.push().setValue(userModel);
                    Toast.makeText(AddUser.this, "User added!", Toast.LENGTH_SHORT).show();
                    email.setText("");
                    password.setText("");
                }
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        };
        mDatabase.orderByChild("email").equalTo(email.getText().toString()).addValueEventListener(valueEventListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mDatabase != null) {
            mDatabase.removeEventListener(valueEventListener);
        }
    }

    ProgressDialog progressDialog;
    public void showProgress(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
    }

    public void hideProgress(){
        if(progressDialog != null)
            progressDialog.hide();
    }
}
