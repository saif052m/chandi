package com.jewelery.chandi.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.jewelery.chandi.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AdminHome extends AppCompatActivity {
    @BindView(R.id.rating)
    Button rating;
    @BindView(R.id.wastage)
    Button wastage;
    @BindView(R.id.making)
    Button making;
    @BindView(R.id.stockReport)
    Button stockReport;
    @BindView(R.id.addStock)
    Button addStock;
    @BindView(R.id.addUser)
    Button addUser;
    @BindView(R.id.logout)
    Button logout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_home);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.rating, R.id.wastage, R.id.making, R.id.stockReport, R.id.addStock, R.id.addUser, R.id.logout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rating:
                Intent intent = new Intent(this, RateSettings.class);
                startActivity(intent);
                break;
            case R.id.wastage:
                showAlert(this, 1);
                break;
            case R.id.making:
                showAlert(this, 2);
                break;
            case R.id.stockReport:
                Intent intent2 = new Intent(this, StockReport.class);
                startActivity(intent2);
                break;
            case R.id.addStock:
                Intent intent3 = new Intent(this, AddStock.class);
                startActivity(intent3);
                break;
            case R.id.addUser:
                Intent intent4 = new Intent(this, AddUser.class);
                startActivity(intent4);
                break;
            case R.id.logout:
                SharedPreferences sp = getSharedPreferences("chandi", Context.MODE_PRIVATE);
                sp.edit().putBoolean("login", false).apply();
                finishAffinity();
                Intent intent5 = new Intent(this, Login.class);
                startActivity(intent5);
                break;
        }
    }

    private void showAlert(Context context, final int type) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final String[] arr = new String[]{"Patti", "Article"};
        builder.setSingleChoiceItems(arr, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(type == 1) {
                    Intent intent1 = new Intent(AdminHome.this, WastageSettings.class);
                    intent1.putExtra("type", arr[i]);
                    startActivity(intent1);
                }
                if(type == 2) {
                    Intent intent1 = new Intent(AdminHome.this, MakingSettings.class);
                    intent1.putExtra("type", arr[i]);
                    startActivity(intent1);
                }
                dialogInterface.dismiss();
            }
        }).show();
    }
}
