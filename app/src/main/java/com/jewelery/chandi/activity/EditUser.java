package com.jewelery.chandi.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jewelery.chandi.R;
import com.jewelery.chandi.model.UserModel;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditUser extends AppCompatActivity {
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.addUser)
    Button addUser;
    private DatabaseReference mDatabase;
    UserModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);
        ButterKnife.bind(this);

        model = (UserModel)getIntent().getSerializableExtra("model");
        email.setText(model.getEmail());
        password.setText(model.getPassword());
    }

    @OnClick(R.id.addUser)
    public void onViewClicked() {
        updateUser();
    }

    private void updateUser() {
        if(email.getText().toString().isEmpty()){
            email.setError("Required");
            return;
        }
        if(password.getText().toString().isEmpty()){
            password.setError("Required");
            return;
        }
        mDatabase = FirebaseDatabase.getInstance().getReference("user").child(model.getKey());
        mDatabase.child("email").setValue(email.getText().toString());
        mDatabase.child("password").setValue(password.getText().toString());
        Toast.makeText(EditUser.this, "User updated!", Toast.LENGTH_SHORT).show();

    }

}
