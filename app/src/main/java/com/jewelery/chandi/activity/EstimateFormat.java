package com.jewelery.chandi.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.print.PrintAttributes;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hp.mss.hpprint.model.ImagePrintItem;
import com.hp.mss.hpprint.model.PDFPrintItem;
import com.hp.mss.hpprint.model.PrintItem;
import com.hp.mss.hpprint.model.PrintJobData;
import com.hp.mss.hpprint.model.PrintMetricsData;
import com.hp.mss.hpprint.model.asset.ImageAsset;
import com.hp.mss.hpprint.model.asset.PDFAsset;
import com.hp.mss.hpprint.util.PrintUtil;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.DottedLineSeparator;
import com.jewelery.chandi.R;
import com.jewelery.chandi.model.NewItemModel;
import com.jewelery.chandi.model.OldItemModel;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EstimateFormat extends AppCompatActivity implements PrintUtil.PrintMetricsListener{
    @BindView(R.id.container1)
    LinearLayout container1;
    @BindView(R.id.container2)
    LinearLayout container2;
    @BindView(R.id.print)
    Button print;
    ArrayList<NewItemModel> newModels = new ArrayList<>();
    ArrayList<OldItemModel> oldModels = new ArrayList<>();
    @BindView(R.id.grandTotal)
    TextView grandTotal;
    @BindView(R.id.oldPanel)
    LinearLayout oldPanel;
    @BindView(R.id.oldPanelLine)
    View oldPanelLine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estimate_format);
        ButterKnife.bind(this);

        newModels = (ArrayList<NewItemModel>) getIntent().getSerializableExtra("new_models");
        oldModels = (ArrayList<OldItemModel>) getIntent().getSerializableExtra("old_models");

        showNewItems();
        showOldItems();
        showGrandTotal();
    }

    private void showGrandTotal() {
        double grandTotal_ = 0;
        for (NewItemModel newModel : newModels) {
            double total = Double.parseDouble(newModel.getTotal());
            grandTotal_ = grandTotal_ + total;
        }
        for (OldItemModel oldModel : oldModels) {
            double total = Double.parseDouble(oldModel.getTotal());
            grandTotal_ = grandTotal_ - total;
        }
        grandTotal.setText("Grand Total: " + String.format("%.2f", grandTotal_));
    }

    private void showNewItems() {
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        for (NewItemModel newModel : newModels) {
            View view = layoutInflater.inflate(R.layout.bill_item, null);
            TextView itemNo = (TextView) view.findViewById(R.id.itemNo);
            TextView itemName = (TextView) view.findViewById(R.id.itemName);
            TextView weight = (TextView) view.findViewById(R.id.weight);
            TextView wastage = (TextView) view.findViewById(R.id.wastage);
            TextView making = (TextView) view.findViewById(R.id.making);
            TextView rate = (TextView) view.findViewById(R.id.rate);
            TextView total = (TextView) view.findViewById(R.id.total);
            itemNo.setText(newModel.getItemNo());
            itemName.setText(newModel.getItemName());
            weight.setText(newModel.getWeight());
            wastage.setText(newModel.getWastage());
            making.setText(newModel.getMaking());
            rate.setText(newModel.getRate());
            total.setText(newModel.getTotal());
            container1.addView(view);
        }
    }

    private void showOldItems() {
        if(oldModels.size() == 0){
            oldPanel.setVisibility(View.GONE);
            oldPanelLine.setVisibility(View.GONE);
            return;
        }
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        for (OldItemModel oldModel : oldModels) {
            View view = layoutInflater.inflate(R.layout.bill_old_item, null);
            TextView itemNo = (TextView) view.findViewById(R.id.itemNo);
            TextView weight = (TextView) view.findViewById(R.id.weight);
            TextView purity = (TextView) view.findViewById(R.id.purity);
            TextView rate = (TextView) view.findViewById(R.id.rate);
            TextView total = (TextView) view.findViewById(R.id.total);
            itemNo.setText(oldModel.getItemNo());
            weight.setText(oldModel.getWeight());
            purity.setText(oldModel.getPurity());
            rate.setText(oldModel.getRate());
            total.setText(oldModel.getTotal());
            container2.addView(view);
        }
    }

    public void printBill(View view) {
        exportToPDF();
    }

    private void exportToPDF() {
        try {
            //Create file path for Pdf
            File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString());
            if (!folder.exists()) {
                Log.e("tag", "not exist");
                folder.mkdir();
            } else {
                Log.e("tag", "exist");
            }
            File file = new File(folder, "bill.pdf");
            // To customise the text of the pdf
            // we can use FontFamily
            Font font1 = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD, new BaseColor(0, 0, 0));
            //Font bf12 = new Font(Font.FontFamily.TIMES_ROMAN, 12);
            // create an instance of itext document
            Document document = new Document(PageSize.A4, 20, 20, 20, 20);
            PdfWriter.getInstance(document, new FileOutputStream(file.getAbsoluteFile()));
            document.open();
            document.addTitle("Bill");
            document.addSubject("Bill");

            // container of all views
            Paragraph preface = new Paragraph();

            // title
            PdfPTable title = new PdfPTable(1);
            title.setWidthPercentage(100);
            PdfPCell cellOne = new PdfPCell(new Phrase("VENKATA SAI JEWELLERS", font1));
            cellOne.setBorder(Rectangle.NO_BORDER);
            cellOne.setHorizontalAlignment(Element.ALIGN_CENTER);
            cellOne.setVerticalAlignment(Element.ALIGN_MIDDLE);
            title.addCell(cellOne);
            preface.add(title);

            // empty line
            /*Paragraph line = new Paragraph("");
            preface.add(line);*/

            // table
            PdfPTable table = new PdfPTable(7);
            table.setWidthPercentage(100);

            // setting table header
            PdfPCell c1 = new PdfPCell(new Phrase("Item No"));
            c1.setBorder(Rectangle.NO_BORDER);
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
            c1.setPadding(3.0f);
            table.addCell(c1);
            PdfPCell c2 = new PdfPCell(new Phrase("Item Name"));
            c2.setBorder(Rectangle.NO_BORDER);
            c2.setHorizontalAlignment(Element.ALIGN_CENTER);
            c2.setVerticalAlignment(Element.ALIGN_MIDDLE);
            c2.setPadding(3.0f);
            table.addCell(c2);
            PdfPCell c3 = new PdfPCell(new Phrase("Weight"));
            c3.setBorder(Rectangle.NO_BORDER);
            c3.setHorizontalAlignment(Element.ALIGN_CENTER);
            c3.setVerticalAlignment(Element.ALIGN_MIDDLE);
            c3.setPadding(3.0f);
            table.addCell(c3);
            PdfPCell c4 = new PdfPCell(new Phrase("Wastage"));
            c4.setBorder(Rectangle.NO_BORDER);
            c4.setHorizontalAlignment(Element.ALIGN_CENTER);
            c4.setVerticalAlignment(Element.ALIGN_MIDDLE);
            c4.setPadding(3.0f);
            table.addCell(c4);
            PdfPCell c5 = new PdfPCell(new Phrase("Making"));
            c5.setBorder(Rectangle.NO_BORDER);
            c5.setHorizontalAlignment(Element.ALIGN_CENTER);
            c5.setVerticalAlignment(Element.ALIGN_MIDDLE);
            c5.setPadding(3.0f);
            table.addCell(c5);
            PdfPCell c6 = new PdfPCell(new Phrase("Rate"));
            c6.setBorder(Rectangle.NO_BORDER);
            c6.setHorizontalAlignment(Element.ALIGN_CENTER);
            c6.setVerticalAlignment(Element.ALIGN_MIDDLE);
            c6.setPadding(3.0f);
            table.addCell(c6);
            PdfPCell c7 = new PdfPCell(new Phrase("Total"));
            c7.setBorder(Rectangle.NO_BORDER);
            c7.setHorizontalAlignment(Element.ALIGN_CENTER);
            c7.setVerticalAlignment(Element.ALIGN_MIDDLE);
            c7.setPadding(3.0f);
            table.addCell(c7);

            table.setHeaderRows(1);
            // setting table values
            for (NewItemModel model : newModels) {
                PdfPCell one = new PdfPCell(new Phrase(model.getItemNo()));
                one.setBorder(Rectangle.NO_BORDER);
                one.setHorizontalAlignment(Element.ALIGN_CENTER);
                one.setVerticalAlignment(Element.ALIGN_MIDDLE);
                one.setPadding(3.0f);
                table.addCell(one);

                PdfPCell two = new PdfPCell(new Phrase(model.getItemName()));
                two.setBorder(Rectangle.NO_BORDER);
                two.setHorizontalAlignment(Element.ALIGN_CENTER);
                two.setVerticalAlignment(Element.ALIGN_MIDDLE);
                two.setPadding(3.0f);
                table.addCell(two);

                PdfPCell three = new PdfPCell(new Phrase(model.getWeight()));
                three.setBorder(Rectangle.NO_BORDER);
                three.setHorizontalAlignment(Element.ALIGN_CENTER);
                three.setVerticalAlignment(Element.ALIGN_MIDDLE);
                three.setPadding(3.0f);
                table.addCell(three);

                PdfPCell four = new PdfPCell(new Phrase(model.getWastage()));
                four.setBorder(Rectangle.NO_BORDER);
                four.setHorizontalAlignment(Element.ALIGN_CENTER);
                four.setVerticalAlignment(Element.ALIGN_MIDDLE);
                four.setPadding(3.0f);
                table.addCell(four);

                PdfPCell five = new PdfPCell(new Phrase(model.getMaking()));
                five.setBorder(Rectangle.NO_BORDER);
                five.setHorizontalAlignment(Element.ALIGN_CENTER);
                five.setVerticalAlignment(Element.ALIGN_MIDDLE);
                five.setPadding(3.0f);
                table.addCell(five);

                PdfPCell six = new PdfPCell(new Phrase(model.getRate()));
                six.setBorder(Rectangle.NO_BORDER);
                six.setHorizontalAlignment(Element.ALIGN_CENTER);
                six.setVerticalAlignment(Element.ALIGN_MIDDLE);
                six.setPadding(3.0f);
                table.addCell(six);

                PdfPCell seven = new PdfPCell(new Phrase(model.getTotal()));
                seven.setBorder(Rectangle.NO_BORDER);
                seven.setHorizontalAlignment(Element.ALIGN_CENTER);
                seven.setVerticalAlignment(Element.ALIGN_MIDDLE);
                seven.setPadding(3.0f);
                table.addCell(seven);
            }
            preface.add(table);

            // empty line
            /*Paragraph line1 = new Paragraph("");
            preface.add(line1);*/

            // line
            DottedLineSeparator separator = new DottedLineSeparator();
            separator.setPercentage(59500f / 523f);
            Chunk linebreak = new Chunk(separator);
            preface.add(linebreak);

            // empty line
            /*Paragraph line2 = new Paragraph("");
            preface.add(line2);*/

            // 2nd table
            PdfPTable table2 = new PdfPTable(5);
            table2.setWidthPercentage(100);

            // setting table header
            PdfPCell c12 = new PdfPCell(new Phrase("Old Item"));
            c12.setBorder(Rectangle.NO_BORDER);
            c12.setHorizontalAlignment(Element.ALIGN_CENTER);
            c12.setVerticalAlignment(Element.ALIGN_MIDDLE);
            c12.setPadding(3.0f);
            table2.addCell(c12);
            PdfPCell c22 = new PdfPCell(new Phrase("Weight"));
            c22.setBorder(Rectangle.NO_BORDER);
            c22.setHorizontalAlignment(Element.ALIGN_CENTER);
            c22.setVerticalAlignment(Element.ALIGN_MIDDLE);
            c22.setPadding(3.0f);
            table2.addCell(c22);
            PdfPCell c32 = new PdfPCell(new Phrase("Purity"));
            c32.setBorder(Rectangle.NO_BORDER);
            c32.setHorizontalAlignment(Element.ALIGN_CENTER);
            c32.setVerticalAlignment(Element.ALIGN_MIDDLE);
            c32.setPadding(3.0f);
            table2.addCell(c32);
            PdfPCell c42 = new PdfPCell(new Phrase("Rate"));
            c42.setBorder(Rectangle.NO_BORDER);
            c42.setHorizontalAlignment(Element.ALIGN_CENTER);
            c42.setVerticalAlignment(Element.ALIGN_MIDDLE);
            c42.setPadding(3.0f);
            table2.addCell(c42);
            PdfPCell c52 = new PdfPCell(new Phrase("Total"));
            c52.setBorder(Rectangle.NO_BORDER);
            c52.setHorizontalAlignment(Element.ALIGN_CENTER);
            c52.setVerticalAlignment(Element.ALIGN_MIDDLE);
            c52.setPadding(3.0f);
            table2.addCell(c52);

            table.setHeaderRows(1);
            // setting table values
            for (OldItemModel model : oldModels) {
                PdfPCell one = new PdfPCell(new Phrase(model.getItemNo()));
                one.setBorder(Rectangle.NO_BORDER);
                one.setHorizontalAlignment(Element.ALIGN_CENTER);
                one.setVerticalAlignment(Element.ALIGN_MIDDLE);
                one.setPadding(3.0f);
                table2.addCell(one);

                PdfPCell two = new PdfPCell(new Phrase(model.getWeight()));
                two.setBorder(Rectangle.NO_BORDER);
                two.setHorizontalAlignment(Element.ALIGN_CENTER);
                two.setVerticalAlignment(Element.ALIGN_MIDDLE);
                two.setPadding(3.0f);
                table2.addCell(two);

                PdfPCell three = new PdfPCell(new Phrase(model.getPurity()));
                three.setBorder(Rectangle.NO_BORDER);
                three.setHorizontalAlignment(Element.ALIGN_CENTER);
                three.setVerticalAlignment(Element.ALIGN_MIDDLE);
                three.setPadding(3.0f);
                table2.addCell(three);

                PdfPCell four = new PdfPCell(new Phrase(model.getRate()));
                four.setBorder(Rectangle.NO_BORDER);
                four.setHorizontalAlignment(Element.ALIGN_CENTER);
                four.setVerticalAlignment(Element.ALIGN_MIDDLE);
                four.setPadding(3.0f);
                table2.addCell(four);

                PdfPCell five = new PdfPCell(new Phrase(model.getTotal()));
                five.setBorder(Rectangle.NO_BORDER);
                five.setHorizontalAlignment(Element.ALIGN_CENTER);
                five.setVerticalAlignment(Element.ALIGN_MIDDLE);
                five.setPadding(3.0f);
                table2.addCell(five);
            }

            if(oldModels.size() > 0) {
                preface.add(table2);

                // empty line
                /*Paragraph line3 = new Paragraph("");
                preface.add(line3);*/

                // line
                DottedLineSeparator separator2 = new DottedLineSeparator();
                separator2.setPercentage(59500f / 523f);
                Chunk linebreak2 = new Chunk(separator2);
                preface.add(linebreak2);

                // empty line
                /*Paragraph line4 = new Paragraph("");
                preface.add(line4);*/
            }

            // grand total
            PdfPTable gTotal = new PdfPTable(1);
            gTotal.setWidthPercentage(100);
            PdfPCell cellOne2 = new PdfPCell(new Phrase(grandTotal.getText().toString(), font1));
            cellOne2.setBorder(Rectangle.NO_BORDER);
            cellOne2.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cellOne2.setVerticalAlignment(Element.ALIGN_MIDDLE);
            gTotal.addCell(cellOne2);
            preface.add(gTotal);

            document.add(preface);
            document.close();
            Toast.makeText(this, "Document Downloaded!", Toast.LENGTH_SHORT).show();
            //kopenPDF(file);
            printDoc(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void printDoc(File file) {
        PDFAsset pdfAsset4x6 = new PDFAsset(file.getAbsolutePath(), false);
        PrintItem printItemDefault = new PDFPrintItem(PrintItem.ScaleType.FIT, pdfAsset4x6);
        PrintJobData printJobData = new PrintJobData(this, printItemDefault);
        printJobData.setJobName("Print");
        //Optionally include print attributes.
        PrintAttributes printAttributes = new PrintAttributes.Builder()
                .setMediaSize(PrintAttributes.MediaSize.NA_LETTER)
                .build();
        printJobData.setPrintDialogOptions(printAttributes);
        PrintUtil.setPrintJobData(printJobData);
        PrintUtil.print(this);
    }

    private void openPDF(File file) {
        Uri uri1 = Uri.fromFile(file);
        Uri uri2 = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".my.package.name.provider", file);
        Log.e("tag", "uri1: " + uri1);
        Log.e("tag", "uri2: " + uri2);
        try {
            Intent intentUrl = new Intent(Intent.ACTION_VIEW);
            if (Build.VERSION.SDK_INT < 24) {
                intentUrl.setDataAndType(uri1, "application/pdf");
            } else {
                intentUrl.setDataAndType(uri2, "application/pdf");
                intentUrl.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }
            intentUrl.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intentUrl);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "No PDF Viewer Installed", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onPrintMetricsDataPosted(PrintMetricsData printMetricsData) {
        Log.e("tag", "onPrintMetricsDataPosted");
    }
}
