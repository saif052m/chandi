package com.jewelery.chandi.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jewelery.chandi.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Login extends AppCompatActivity {
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.admin)
    RadioButton admin;
    @BindView(R.id.salesMan)
    RadioButton salesMan;
    @BindView(R.id.type)
    RadioGroup type;
    @BindView(R.id.login)
    Button login;
    private DatabaseReference mDatabase;
    private ValueEventListener valueEventListener;

    // db path
    // https://chandi-98c69.firebaseio.com/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        ButterKnife.bind(this);

        SharedPreferences sp = getSharedPreferences("chandi", Context.MODE_PRIVATE);
        if(sp.getBoolean("login", false)){
            if(sp.getString("type", "").equalsIgnoreCase("user")){
                Intent intent = new Intent(Login.this, UserHome.class);
                startActivity(intent);
                finish();
            }
            else{
                Intent intent = new Intent(Login.this, AdminHome.class);
                startActivity(intent);
                finish();
            }
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 100);
        }
        else if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 100);
        }
        else if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
        }
    }

    private void test() {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("user");
        String uid = mDatabase.push().getKey();
        mDatabase.child(uid).child("email").setValue("test");
        mDatabase.child(uid).child("password").setValue("test");
    }

    @OnClick(R.id.login)
    public void onViewClicked() {
        if(email.getText().toString().isEmpty()){
            email.setError("Required");
        }
        else if(password.getText().toString().isEmpty()){
            password.setError("Required");
        }
        else{
            if(type.getCheckedRadioButtonId()==R.id.admin){
                loginAdmin();
            }
            else{
                loginUser();
            }
        }
    }

    private void loginAdmin() {
        showProgress();
        mDatabase = FirebaseDatabase.getInstance().getReference("admin");
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                hideProgress();
                String email_ = (String) dataSnapshot.child("email").getValue();
                String password_ = (String) dataSnapshot.child("password").getValue();
                if(!email_.equals(email.getText().toString())){
                    Toast.makeText(Login.this, "Incorrect data!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(!password_.equals(password.getText().toString())){
                    Toast.makeText(Login.this, "Incorrect data!", Toast.LENGTH_SHORT).show();
                    return;
                }
                mDatabase.removeEventListener(valueEventListener);
                SharedPreferences sp = getSharedPreferences("chandi", Context.MODE_PRIVATE);
                sp.edit().putBoolean("login", true).apply();
                sp.edit().putString("type", "admin").apply();
                Intent intent = new Intent(Login.this, AdminHome.class);
                startActivity(intent);
                finish();
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        };
        mDatabase.addValueEventListener(valueEventListener);
    }

    private void loginUser() {
        showProgress();
        mDatabase = FirebaseDatabase.getInstance().getReference("user");
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                hideProgress();
                boolean flag = false;
                String uid = "";
                for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                    String email_ = (String) snapshot.child("email").getValue();
                    String password_ = (String) snapshot.child("password").getValue();
                    uid = (String) snapshot.child("id").getValue();
                    if(email_.equals(email.getText().toString()) && password_.equals(password.getText().toString())){
                        flag = true;
                        break;
                    }
                }

                if(!flag){
                    Toast.makeText(Login.this, "Incorrect data!", Toast.LENGTH_SHORT).show();
                    return;
                }
                mDatabase.removeEventListener(valueEventListener);
                SharedPreferences sp = getSharedPreferences("chandi", Context.MODE_PRIVATE);
                sp.edit().putBoolean("login", true).apply();
                sp.edit().putString("type", "user").apply();
                sp.edit().putString("uid", uid).apply();
                Log.e("tag", "uid: "+uid);
                Intent intent = new Intent(Login.this, UserHome.class);
                startActivity(intent);
                finish();
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        };
        mDatabase.addValueEventListener(valueEventListener);
    }

    ProgressDialog progressDialog;
    public void showProgress(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
    }

    public void hideProgress(){
        if(progressDialog != null)
            progressDialog.hide();
    }
}
