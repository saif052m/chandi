package com.jewelery.chandi.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jewelery.chandi.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MakingSettings extends AppCompatActivity {
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.container)
    LinearLayout container;
    @BindView(R.id.save)
    Button save;
    String type;
    private DatabaseReference mDatabase;
    private ValueEventListener valueEventListener;
    ArrayList<View> views = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_making_settings);
        ButterKnife.bind(this);

        type = getIntent().getStringExtra("type");
        title.setText("Making Charge Chart for "+type);

        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.wastage_percentage_item, null);
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText("0-10 gm");
        TextView percentage = (TextView) view.findViewById(R.id.percentage);
        percentage.setVisibility(View.GONE);
        views.add(view);
        container.addView(view);

        for(int i=1; i<=30; i++){
            View viewN = layoutInflater.inflate(R.layout.wastage_percentage_item, null);
            TextView titleN = (TextView) viewN.findViewById(R.id.title);
            titleN.setText(((i*10)+1)+"-"+((i*10)+10)+" gm");
            TextView percentageN = (TextView) viewN.findViewById(R.id.percentage);
            percentageN.setVisibility(View.GONE);
            if(i==30){
                titleN.setText("300+ gm");
            }
            views.add(viewN);
            container.addView(viewN);
        }

        dbConnect();
    }

    private void dbConnect() {
        showProgress();
        mDatabase = FirebaseDatabase.getInstance().getReference("making-setting").child(type.toLowerCase()+"-making");
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                hideProgress();
                if(dataSnapshot == null)
                    return;
                for(DataSnapshot snapshot: dataSnapshot.getChildren()){
                    for (int i = 0; i < views.size(); i++) {
                        View view = views.get(i);
                        TextView title = (TextView) view.findViewById(R.id.title);
                        EditText value = (EditText) view.findViewById(R.id.value);
                        if(snapshot.getKey().equalsIgnoreCase(title.getText().toString())) {
                            value.setText((String) snapshot.getValue());
                            break;
                        }
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        };
        mDatabase.addValueEventListener(valueEventListener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mDatabase != null) {
            mDatabase.removeEventListener(valueEventListener);
        }
    }

    @OnClick(R.id.save)
    public void onViewClicked() {
        for (int i = 0; i < views.size(); i++) {
            View view = views.get(i);
            TextView title = (TextView) view.findViewById(R.id.title);
            EditText value = (EditText) view.findViewById(R.id.value);
            if(value.getText().toString().equals("0")){
                value.setError("Should be greater than 0");
                return;
            }
        }

        for (int i = 0; i < views.size(); i++) {
            View view = views.get(i);
            TextView title = (TextView) view.findViewById(R.id.title);
            EditText value = (EditText) view.findViewById(R.id.value);
            mDatabase.child(title.getText().toString()).setValue(value.getText().toString());
        }

        Toast.makeText(MakingSettings.this, "Data Saved!", Toast.LENGTH_SHORT).show();
    }

    ProgressDialog progressDialog;
    public void showProgress(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
    }

    public void hideProgress(){
        if(progressDialog != null)
            progressDialog.hide();
    }
}
