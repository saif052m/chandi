package com.jewelery.chandi.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jewelery.chandi.R;
import com.jewelery.chandi.model.RateModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RateSettings extends AppCompatActivity {
    @BindView(R.id.pattiRate)
    EditText pattiRate;
    @BindView(R.id.articleRate)
    EditText articleRate;
    @BindView(R.id.pureRate)
    EditText pureRate;
    @BindView(R.id.save)
    Button save;
    @BindView(R.id.oldItemRate)
    EditText oldItemRate;
    private DatabaseReference mDatabase;
    private ValueEventListener valueEventListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_settings);
        ButterKnife.bind(this);

        dbConnect();
    }

    private void dbConnect() {
        showProgress();
        mDatabase = FirebaseDatabase.getInstance().getReference("rate-setting");
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                hideProgress();
                if(dataSnapshot == null)
                    return;
                RateModel model = dataSnapshot.getValue(RateModel.class);
                if(model == null)
                    return;
                pattiRate.setText(model.getPattiRate());
                articleRate.setText(model.getArticleRate());
                pureRate.setText(model.getPureRate());
                oldItemRate.setText(model.getOldItemRate());
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        };
        mDatabase.addValueEventListener(valueEventListener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mDatabase != null) {
            mDatabase.removeEventListener(valueEventListener);
        }
    }

    @OnClick(R.id.save)
    public void onViewClicked() {
        if(pattiRate.getText().toString().equals("0")){
            pattiRate.setError("Should be greater than 0");
            return;
        }
        if(articleRate.getText().toString().equals("0")){
            articleRate.setError("Should be greater than 0");
            return;
        }
        if(pureRate.getText().toString().equals("0")){
            pureRate.setError("Should be greater than 0");
            return;
        }
        if(oldItemRate.getText().toString().equals("0")){
            oldItemRate.setError("Should be greater than 0");
            return;
        }

        RateModel model = new RateModel();
        model.setPattiRate(pattiRate.getText().toString());
        model.setArticleRate(articleRate.getText().toString());
        model.setPureRate(pureRate.getText().toString());
        model.setOldItemRate(oldItemRate.getText().toString());
        mDatabase.setValue(model);
        Toast.makeText(RateSettings.this, "Data saved!", Toast.LENGTH_SHORT).show();
    }

    ProgressDialog progressDialog;

    public void showProgress() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
    }

    public void hideProgress() {
        if (progressDialog != null)
            progressDialog.hide();
    }
}
