package com.jewelery.chandi.activity;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jewelery.chandi.R;
import com.jewelery.chandi.model.SaleModel;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

public class StockReport extends AppCompatActivity {
    @BindView(R.id.container)
    LinearLayout container;
    @BindView(R.id.print)
    Button print;
    private DatabaseReference mDatabase;
    private ValueEventListener valueEventListener;
    ArrayList<View> views = new ArrayList<>();
    ArrayList<SaleModel> models = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_report);
        ButterKnife.bind(this);

        dbConnect();
        //test();
    }

    private void test() {
        SaleModel model=new SaleModel();
        model.setDate("date");
        model.setDesignNo("dNo");
        model.setItem("item");
        model.setSize("size");
        model.setSmId("smId");
        model.setWeight("weight");
        model.setSno("");
        mDatabase.push().setValue(model);
    }

    private void dbConnect() {
        showProgress();
        mDatabase = FirebaseDatabase.getInstance().getReference("sale");
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                hideProgress();
                views.clear();
                container.removeAllViews();
                models.clear();
                if(dataSnapshot == null)
                    return;
                int i=0;
                for(DataSnapshot snapshot: dataSnapshot.getChildren()){
                    SaleModel model = snapshot.getValue(SaleModel.class);
                    if(model == null)
                        return;
                    LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                    View view = layoutInflater.inflate(R.layout.sales_item, null);
                    TextView sno= (TextView) view.findViewById(R.id.sNo);
                    TextView smId= (TextView) view.findViewById(R.id.smId);
                    TextView date= (TextView) view.findViewById(R.id.date);
                    TextView item= (TextView) view.findViewById(R.id.item);
                    TextView weight= (TextView) view.findViewById(R.id.weight);
                    TextView designNo= (TextView) view.findViewById(R.id.designNo);
                    TextView size= (TextView) view.findViewById(R.id.size);

                    sno.setText(""+(i+1));
                    model.setSno(""+(i+1));
                    smId.setText(model.getSmId());
                    date.setText(model.getDate());
                    item.setText(model.getItem());
                    weight.setText(model.getWeight());
                    designNo.setText(model.getDesignNo());
                    size.setText(model.getSize());
                    container.addView(view);
                    models.add(model);
                    i++;
                }
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        };
        mDatabase.addValueEventListener(valueEventListener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mDatabase != null) {
            mDatabase.removeEventListener(valueEventListener);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @OnClick(R.id.print)
    public void onViewClicked() {
        if(models.size() > 0) {
            // download excel file
            createExcelFile(models);
        }
    }

    public void clearSales(View view) {
        if(models.size() > 0) {
            showAlert(this);
        }
    }

    private void showAlert(Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Do you want to clear sales data?")
            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    mDatabase.setValue(null);
                }
            })
            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            })
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show();
    }

    ProgressDialog progressDialog;
    public void showProgress(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
    }

    public void hideProgress(){
        if(progressDialog != null)
            progressDialog.hide();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void createExcelFile(ArrayList<SaleModel> models){
        try {
            File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString());
            if(!folder.exists()){
                Log.e("tag", "not exist");
                folder.mkdir();
            }
            else {
                Log.e("tag", "exist");
            }
            String name = "sales-"+new SimpleDateFormat("dd-MM-yyyy").format(System.currentTimeMillis());
            File file = new File(folder, name+".xlsx");
            WritableWorkbook workbook = Workbook.createWorkbook(file);
            WritableSheet sheet = workbook.createSheet("Sheet", 0);
            sheet.addColumnPageBreak(7);
            sheet.addRowPageBreak(models.size());
            // add headings
            Label l1 = new Label(0, 0, "S.No");
            Label l2 = new Label(1, 0, "SM. Id");
            Label l3 = new Label(2, 0, "Date");
            Label l4 = new Label(3, 0, "Item");
            Label l5 = new Label(4, 0, "Weight");
            Label l6 = new Label(5, 0, "Design No.");
            Label l7 = new Label(6, 0, "Size");
            sheet.addCell(l1);
            sheet.addCell(l2);
            sheet.addCell(l3);
            sheet.addCell(l4);
            sheet.addCell(l5);
            sheet.addCell(l6);
            sheet.addCell(l7);
            for (int i = 0; i < models.size(); i++) {
                // add values
                l1 = new Label(0, i+1, models.get(i).getSno());
                l2 = new Label(1, i+1, models.get(i).getSmId());
                l3 = new Label(2, i+1, models.get(i).getDate());
                l4 = new Label(3, i+1, models.get(i).getItem());
                l5 = new Label(4, i+1, models.get(i).getWeight());
                l6 = new Label(5, i+1, models.get(i).getDesignNo());
                l7 = new Label(6, i+1, models.get(i).getSize());
                sheet.addCell(l1);
                sheet.addCell(l2);
                sheet.addCell(l3);
                sheet.addCell(l4);
                sheet.addCell(l5);
                sheet.addCell(l6);
                sheet.addCell(l7);
            }
            workbook.write();
            workbook.close();
            openExcelFile(file.getAbsolutePath());
            //sendMailWithFile(file.getAbsolutePath());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openExcelFile(String path) {
        Uri uri1  = Uri.parse(path);
        Uri uri2 = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".my.package.name.provider", new File(path));
        Intent intent = new Intent(Intent.ACTION_VIEW);
        if(Build.VERSION.SDK_INT < 24) {
            Log.e("tag", "uri1");
            intent.setDataAndType(uri1, "application/vnd.ms-excel");
        }
        else{
            Log.e("tag", "uri2");
            intent.setDataAndType(uri2, "application/vnd.ms-excel");
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        try {
            startActivity(intent);
        }
        catch (ActivityNotFoundException e) {
            Toast.makeText(this, "No Application Available to View Excel File.", Toast.LENGTH_SHORT).show();
        }
    }

}
