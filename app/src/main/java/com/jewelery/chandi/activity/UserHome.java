package com.jewelery.chandi.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jewelery.chandi.R;
import com.jewelery.chandi.model.NewItemModel;
import com.jewelery.chandi.model.OldItemModel;
import com.jewelery.chandi.model.RateModel;
import com.jewelery.chandi.model.SaleModel;
import com.jewelery.chandi.model.StockModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserHome extends AppCompatActivity {
    @BindView(R.id.container)
    LinearLayout container;
    @BindView(R.id.addItem)
    TextView addItem;
    @BindView(R.id.addOldItem)
    TextView addOldItem;
    @BindView(R.id.estimate)
    Button estimate;
    @BindView(R.id.bill)
    Button bill;
    ArrayList<LinearLayout> items = new ArrayList<>();
    ArrayList<LinearLayout> oldItems = new ArrayList<>();
    ArrayList<NewItemModel> newModels = new ArrayList<>();
    ArrayList<OldItemModel> oldModels = new ArrayList<>();
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.namePanel)
    LinearLayout namePanel;
    @BindView(R.id.mobile)
    EditText mobile;
    @BindView(R.id.mobilePanel)
    LinearLayout mobilePanel;
    @BindView(R.id.address)
    EditText address;
    @BindView(R.id.addressPanel)
    LinearLayout addressPanel;
    @BindView(R.id.detailPanel)
    LinearLayout detailPanel;
    @BindView(R.id.logout)
    Button logout;
    private DatabaseReference rateRef;
    private ValueEventListener rateListener;
    private DatabaseReference wastageRef;
    private ValueEventListener wastageListener;
    private DatabaseReference makingRef;
    private ValueEventListener makingListener;
    private DatabaseReference stockRef;
    private ValueEventListener stockListener;
    HashMap<String, String> pattiWastage = new HashMap<>();
    HashMap<String, String> articleWastage = new HashMap<>();
    HashMap<String, String> pattiMaking = new HashMap<>();
    HashMap<String, String> articleMaking = new HashMap<>();
    RateModel rateModel;
    StockModel stockModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_home);
        ButterKnife.bind(this);

        addInitialItem();

        loadRates();
        loadWastage();
        loadMaking();
        loadStock();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(rateRef != null)
            rateRef.removeEventListener(rateListener);
        if(wastageRef != null)
            wastageRef.removeEventListener(wastageListener);
        if(makingRef != null)
            makingRef.removeEventListener(makingListener);
        if(stockRef != null)
            stockRef.removeEventListener(stockListener);
    }

    private void loadStock() {
        stockRef = FirebaseDatabase.getInstance().getReference("stock");
        stockListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot == null)
                    return;
                stockModel = dataSnapshot.getValue(StockModel.class);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        };
        stockRef.addValueEventListener(stockListener);
    }

    private void loadRates() {
        rateRef = FirebaseDatabase.getInstance().getReference("rate-setting");
        rateListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot == null)
                    return;
                rateModel = dataSnapshot.getValue(RateModel.class);

                //rateRef.removeEventListener(rateListener);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        };
        rateRef.addValueEventListener(rateListener);
    }

    private void loadWastage() {
        wastageRef = FirebaseDatabase.getInstance().getReference("wastage-setting");
        wastageListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot == null)
                    return;
                for(DataSnapshot snapshot: dataSnapshot.getChildren()){
                    if(snapshot.getKey().equalsIgnoreCase("patti-wastage")){
                        // patti
                        for(DataSnapshot snapshot1: snapshot.getChildren()){
                            pattiWastage.put(snapshot1.getKey(), (String) snapshot1.getValue());
                        }
                    }
                    else{
                        // article
                        for(DataSnapshot snapshot1: snapshot.getChildren()){
                            articleWastage.put(snapshot1.getKey(), (String) snapshot1.getValue());
                        }
                    }
                }
                //wastageRef.removeEventListener(wastageListener);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        };
        wastageRef.addValueEventListener(wastageListener);
    }

    private void loadMaking() {
        makingRef = FirebaseDatabase.getInstance().getReference("making-setting");
        makingListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot == null)
                    return;
                for(DataSnapshot snapshot: dataSnapshot.getChildren()){
                    if(snapshot.getKey().equalsIgnoreCase("patti-making")){
                        // patti
                        for(DataSnapshot snapshot1: snapshot.getChildren()){
                            pattiMaking.put(snapshot1.getKey(), (String) snapshot1.getValue());
                        }
                    }
                    else{
                        // article
                        for(DataSnapshot snapshot1: snapshot.getChildren()){
                            articleMaking.put(snapshot1.getKey(), (String) snapshot1.getValue());
                        }
                    }
                }
                //makingRef.removeEventListener(makingListener);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        };
        makingRef.addValueEventListener(makingListener);
    }

    private void addInitialItem() {
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        LinearLayout ll = (LinearLayout) layoutInflater.inflate(R.layout.add_item_layout, null);
        final RadioGroup type = (RadioGroup) ll.findViewById(R.id.type);
        final LinearLayout weightPanel = (LinearLayout) ll.findViewById(R.id.weightPanel);
        final LinearLayout designNoPanel = (LinearLayout) ll.findViewById(R.id.designNoPanel);
        final LinearLayout sizePanel = (LinearLayout) ll.findViewById(R.id.sizePanel);
        EditText weight = (EditText) ll.findViewById(R.id.weight);
        EditText designNo = (EditText) ll.findViewById(R.id.designNo);
        EditText size = (EditText) ll.findViewById(R.id.size);
        ImageView remove = (ImageView) ll.findViewById(R.id.remove);
        remove.setVisibility(View.GONE);
        type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (type.getCheckedRadioButtonId() == R.id.patti) {
                    weightPanel.setVisibility(View.VISIBLE);
                    designNoPanel.setVisibility(View.VISIBLE);
                    sizePanel.setVisibility(View.VISIBLE);
                } else if (type.getCheckedRadioButtonId() == R.id.article) {
                    weightPanel.setVisibility(View.VISIBLE);
                    designNoPanel.setVisibility(View.VISIBLE);
                    sizePanel.setVisibility(View.VISIBLE);
                } else if (type.getCheckedRadioButtonId() == R.id.pure) {
                    weightPanel.setVisibility(View.VISIBLE);
                    designNoPanel.setVisibility(View.GONE);
                    sizePanel.setVisibility(View.GONE);
                }
            }
        });
        container.addView(ll);
        items.add(ll);
    }

    @OnClick({R.id.addItem, R.id.addOldItem, R.id.estimate, R.id.bill, R.id.logout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.addItem:
                addItem();
                break;
            case R.id.addOldItem:
                addOldItem();
                break;
            case R.id.estimate:
                if(rateModel == null)
                    return;

                validateFormForEstimate();
                break;
            case R.id.bill:
                if(rateModel == null)
                    return;

                if (!detailPanel.isShown()) {
                    detailPanel.setVisibility(View.VISIBLE);
                    return;
                }
                validateFormForBill();
                break;
            case R.id.logout:
                SharedPreferences sp = getSharedPreferences("chandi", Context.MODE_PRIVATE);
                sp.edit().putBoolean("login", false).apply();
                finishAffinity();
                Intent intent5 = new Intent(this, Login.class);
                startActivity(intent5);
                break;
        }
    }

    private void validateFormForEstimate() {
        newModels.clear();
        oldModels.clear();
        for (int i = 0; i < items.size(); i++) {
            final LinearLayout ll = items.get(i);
            final RadioGroup type = (RadioGroup) ll.findViewById(R.id.type);
            EditText weight = (EditText) ll.findViewById(R.id.weight);
            EditText designNo = (EditText) ll.findViewById(R.id.designNo);
            EditText size = (EditText) ll.findViewById(R.id.size);
            if (weight.getText().toString().isEmpty()) {
                weight.setError("Required");
                return;
            }

            NewItemModel model = new NewItemModel();
            model.setItemNo(""+(i+1));
            if(type.getCheckedRadioButtonId() == R.id.patti){
                model.setItemName("Patti");
                model.setRate(rateModel.getPattiRate());
                model.setWastage(calculateWastage("patti", weight.getText().toString()));
                model.setMaking(calculateMaking("patti", weight.getText().toString()));
            }
            else if(type.getCheckedRadioButtonId() == R.id.article){
                model.setItemName("Article");
                model.setRate(rateModel.getArticleRate());
                model.setWastage(calculateWastage("article", weight.getText().toString()));
                model.setMaking(calculateMaking("article", weight.getText().toString()));
            }
            else if(type.getCheckedRadioButtonId() == R.id.pure){
                model.setItemName("Pure");
                model.setRate(rateModel.getPureRate());
                model.setWastage("0");
                model.setMaking("0");
            }
            model.setWeight(weight.getText().toString());
            model.setDesignNo(designNo.getText().toString());
            model.setSize(size.getText().toString());
            model.setTotal(calculateNewItemPrice(model));
            newModels.add(model);
        }

        for (int i = 0; i < oldItems.size(); i++) {
            final LinearLayout ll = oldItems.get(i);
            EditText weight = (EditText) ll.findViewById(R.id.weight);
            EditText purity = (EditText) ll.findViewById(R.id.purity);
            if (weight.getText().toString().isEmpty()) {
                weight.setError("Required");
                return;
            }
            if (purity.getText().toString().isEmpty()) {
                purity.setError("Required");
                return;
            }
            OldItemModel model = new OldItemModel();
            model.setItemNo(""+(i+1));
            model.setRate(rateModel.getOldItemRate());
            model.setWeight(weight.getText().toString());
            model.setPurity(purity.getText().toString());
            model.setTotal(calculateOldItemPrice(model));
            oldModels.add(model);
        }

        Intent intent = new Intent(this, EstimateFormat.class);
        intent.putExtra("new_models", newModels);
        intent.putExtra("old_models", oldModels);
        startActivity(intent);
    }

    private String calculateOldItemPrice(OldItemModel model) {
        double weight = Double.parseDouble(model.getWeight());
        double purity = Double.parseDouble(model.getPurity());
        double rate = Double.parseDouble(model.getRate());
        double total = weight * (purity/100) * rate;
        return String.format("%.2f", total);
    }

    private String calculateNewItemPrice(NewItemModel model) {
        double weight = Double.parseDouble(model.getWeight());
        double wastage = Double.parseDouble(model.getWastage());
        double rate = Double.parseDouble(model.getRate());
        double making = Double.parseDouble(model.getMaking());
        double total = 0;
        if(model.getItemName().equalsIgnoreCase("Patti")){
            total = (weight+wastage)*rate+making;
        }
        else if(model.getItemName().equalsIgnoreCase("Article")){
            total = (weight+wastage)*rate+making;
        }
        else if(model.getItemName().equalsIgnoreCase("Pure")){
            total = weight*rate;
        }
        return String.format("%.2f", total);
    }

    private String calculateMaking(String type, String w) {
        if(type.equalsIgnoreCase("patti")){
            // patti
            double weight = Double.parseDouble(w);
            Set<String> keySet = pattiMaking.keySet();
            for (String key : keySet) {
                if(key.equalsIgnoreCase("300+ gm")){
                    if(weight > 300)
                        return pattiMaking.get(key);
                }
                else {
                    String key1 = key.replace(" ", "");
                    String key2 = key1.replace("gm", "");
                    String [] arr = key2.split("-");
                    double start = Double.parseDouble(arr[0]);
                    double end = Double.parseDouble(arr[1]);
                    if(weight>=start && weight<=end){
                        return pattiMaking.get(key);
                    }
                }
            }
        }
        else{
            // article
            double weight = Double.parseDouble(w);
            Set<String> keySet = articleMaking.keySet();
            for (String key : keySet) {
                if(key.equalsIgnoreCase("300+ gm")){
                    if(weight > 300)
                        return articleMaking.get(key);
                }
                else {
                    String key1 = key.replace(" ", "");
                    String key2 = key1.replace("gm", "");
                    String [] arr = key2.split("-");
                    double start = Double.parseDouble(arr[0]);
                    double end = Double.parseDouble(arr[1]);
                    if(weight>=start && weight<=end){
                        return articleMaking.get(key);
                    }
                }
            }
        }
        return null;
    }

    private String calculateWastage(String type, String w) {
        if(type.equalsIgnoreCase("patti")){
            // patti
            double weight = Double.parseDouble(w);
            Set<String> keySet = pattiWastage.keySet();
            for (String key : keySet) {
                if(key.equalsIgnoreCase("300+ gm")){
                    if(weight > 300)
                        return pattiWastage.get(key);
                }
                else {
                    String key1 = key.replace(" ", "");
                    String key2 = key1.replace("gm", "");
                    String [] arr = key2.split("-");
                    double start = Double.parseDouble(arr[0]);
                    double end = Double.parseDouble(arr[1]);
                    if(weight>=start && weight<=end){
                        return pattiWastage.get(key);
                    }
                }
            }
        }
        else{
            // article
            double weight = Double.parseDouble(w);
            Set<String> keySet = articleWastage.keySet();
            for (String key : keySet) {
                if(key.equalsIgnoreCase("300+ gm")){
                    if(weight > 300)
                        return articleWastage.get(key);
                }
                else {
                    String key1 = key.replace(" ", "");
                    String key2 = key1.replace("gm", "");
                    String [] arr = key2.split("-");
                    double start = Double.parseDouble(arr[0]);
                    double end = Double.parseDouble(arr[1]);
                    if(weight>=start && weight<=end){
                        return articleWastage.get(key);
                    }
                }
            }
        }
        return null;
    }

    private void validateFormForBill() {
        newModels.clear();
        oldModels.clear();
        for (int i = 0; i < items.size(); i++) {
            final LinearLayout ll = items.get(i);
            final RadioGroup type = (RadioGroup) ll.findViewById(R.id.type);
            EditText weight = (EditText) ll.findViewById(R.id.weight);
            EditText designNo = (EditText) ll.findViewById(R.id.designNo);
            EditText size = (EditText) ll.findViewById(R.id.size);
            if (weight.getText().toString().isEmpty()) {
                weight.setError("Required");
                return;
            }

            NewItemModel model = new NewItemModel();
            model.setItemNo(""+(i+1));
            if(type.getCheckedRadioButtonId() == R.id.patti){
                model.setItemName("Patti");
                model.setRate(rateModel.getPattiRate());
                model.setWastage(calculateWastage("patti", weight.getText().toString()));
                model.setMaking(calculateMaking("patti", weight.getText().toString()));
            }
            else if(type.getCheckedRadioButtonId() == R.id.article){
                model.setItemName("Article");
                model.setRate(rateModel.getArticleRate());
                model.setWastage(calculateWastage("article", weight.getText().toString()));
                model.setMaking(calculateMaking("article", weight.getText().toString()));
            }
            else if(type.getCheckedRadioButtonId() == R.id.pure){
                model.setItemName("Pure");
                model.setRate(rateModel.getPureRate());
                model.setWastage("0");
                model.setMaking("0");
            }
            model.setWeight(weight.getText().toString());
            model.setDesignNo(designNo.getText().toString());
            model.setSize(size.getText().toString());
            model.setTotal(calculateNewItemPrice(model));
            newModels.add(model);
        }

        for (int i = 0; i < oldItems.size(); i++) {
            final LinearLayout ll = oldItems.get(i);
            EditText weight = (EditText) ll.findViewById(R.id.weight);
            EditText purity = (EditText) ll.findViewById(R.id.purity);
            if (weight.getText().toString().isEmpty()) {
                weight.setError("Required");
                return;
            }
            if (purity.getText().toString().isEmpty()) {
                purity.setError("Required");
                return;
            }
            OldItemModel model = new OldItemModel();
            model.setItemNo(""+(i+1));
            model.setRate(rateModel.getOldItemRate());
            model.setWeight(weight.getText().toString());
            model.setPurity(purity.getText().toString());
            model.setTotal(calculateOldItemPrice(model));
            oldModels.add(model);
        }

        // make sale
        DatabaseReference saleRef = FirebaseDatabase.getInstance().getReference("sale");
        SharedPreferences sp = getSharedPreferences("chandi", Context.MODE_PRIVATE);
        String smId = sp.getString("uid", "");
        for (NewItemModel newModel : newModels) {
            SaleModel model=new SaleModel();
            model.setDate(new SimpleDateFormat("dd-MM-yyyy").format(System.currentTimeMillis()));
            model.setDesignNo(newModel.getDesignNo());
            model.setItem(newModel.getItemName());
            model.setSize(newModel.getSize());
            model.setSmId(smId);
            model.setWeight(newModel.getWeight());
            model.setSno("");
            saleRef.push().setValue(model);
        }
        for (OldItemModel oldModel : oldModels) {
            SaleModel model=new SaleModel();
            model.setDate(new SimpleDateFormat("dd-MM-yyyy").format(System.currentTimeMillis()));
            model.setDesignNo("");
            model.setItem("Old");
            model.setSize("");
            model.setSmId(smId);
            model.setWeight(oldModel.getWeight());
            model.setSno("");
            saleRef.push().setValue(model);
        }

        Intent intent = new Intent(this, BillFormat.class);
        intent.putExtra("new_models", newModels);
        intent.putExtra("old_models", oldModels);
        intent.putExtra("name", name.getText().toString());
        intent.putExtra("mobile", mobile.getText().toString());
        intent.putExtra("address", address.getText().toString());
        startActivity(intent);
    }

    private void addItem() {
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        final LinearLayout ll = (LinearLayout) layoutInflater.inflate(R.layout.add_item_layout, null);
        final RadioGroup type = (RadioGroup) ll.findViewById(R.id.type);
        final LinearLayout weightPanel = (LinearLayout) ll.findViewById(R.id.weightPanel);
        final LinearLayout designNoPanel = (LinearLayout) ll.findViewById(R.id.designNoPanel);
        final LinearLayout sizePanel = (LinearLayout) ll.findViewById(R.id.sizePanel);
        EditText weight = (EditText) ll.findViewById(R.id.weight);
        EditText designNo = (EditText) ll.findViewById(R.id.designNo);
        EditText size = (EditText) ll.findViewById(R.id.size);
        ImageView remove = (ImageView) ll.findViewById(R.id.remove);
        type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (type.getCheckedRadioButtonId() == R.id.patti) {
                    weightPanel.setVisibility(View.VISIBLE);
                    designNoPanel.setVisibility(View.VISIBLE);
                    sizePanel.setVisibility(View.VISIBLE);
                } else if (type.getCheckedRadioButtonId() == R.id.article) {
                    weightPanel.setVisibility(View.VISIBLE);
                    designNoPanel.setVisibility(View.VISIBLE);
                    sizePanel.setVisibility(View.VISIBLE);
                } else if (type.getCheckedRadioButtonId() == R.id.pure) {
                    weightPanel.setVisibility(View.VISIBLE);
                    designNoPanel.setVisibility(View.GONE);
                    sizePanel.setVisibility(View.GONE);
                }
            }
        });
        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                container.removeView(ll);
                items.remove(ll);
            }
        });
        container.addView(ll);
        items.add(ll);
    }

    private void addOldItem() {
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        final LinearLayout ll = (LinearLayout) layoutInflater.inflate(R.layout.add_old_item_layout, null);
        final LinearLayout weightPanel = (LinearLayout) ll.findViewById(R.id.weightPanel);
        final LinearLayout purityPanel = (LinearLayout) ll.findViewById(R.id.purityPanel);
        EditText weight = (EditText) ll.findViewById(R.id.weight);
        EditText purity = (EditText) ll.findViewById(R.id.purity);
        ImageView remove = (ImageView) ll.findViewById(R.id.remove);
        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                container.removeView(ll);
                oldItems.remove(ll);
            }
        });
        container.addView(ll);
        oldItems.add(ll);
    }
}
