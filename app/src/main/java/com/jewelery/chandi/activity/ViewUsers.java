package com.jewelery.chandi.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jewelery.chandi.R;
import com.jewelery.chandi.adapter.UserAdapter;
import com.jewelery.chandi.model.SaleModel;
import com.jewelery.chandi.model.UserModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewUsers extends AppCompatActivity {
    @BindView(R.id.noresult)
    TextView noresult;
    @BindView(R.id.listView)
    ListView listView;
    UserAdapter userAdapter;
    ArrayList<UserModel> userModels = new ArrayList<>();
    private DatabaseReference mDatabase;
    private ValueEventListener valueEventListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_users);
        ButterKnife.bind(this);

        dbConnect();
    }

    private void dbConnect() {
        showProgress();
        mDatabase = FirebaseDatabase.getInstance().getReference("user");
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                hideProgress();
                userModels.clear();
                for(DataSnapshot snapshot: dataSnapshot.getChildren()){
                    UserModel userModel = snapshot.getValue(UserModel.class);
                    if(userModel != null) {
                        userModel.setKey(snapshot.getKey());
                        userModels.add(userModel);
                    }
                }

                if(userModels.size() == 0){
                    Toast.makeText(ViewUsers.this, "No users found!", Toast.LENGTH_SHORT).show();
                }

                userAdapter = new UserAdapter(ViewUsers.this, userModels);
                listView.setAdapter(userAdapter);
                listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int pos, long l) {
                        showAlert(ViewUsers.this, userModels.get(pos));
                        return false;
                    }
                });
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        };
        mDatabase.addValueEventListener(valueEventListener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mDatabase != null) {
            mDatabase.removeEventListener(valueEventListener);
        }
    }

    private void showAlert(Context context, final UserModel userModel){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final String [] arr = new String[]{"Edit", "Delete"};
        builder.setSingleChoiceItems(arr, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                if(i == 0){
                    Intent intent = new Intent(ViewUsers.this, EditUser.class);
                    intent.putExtra("model", userModel);
                    startActivity(intent);
                }
                else{
                    deleteUser(userModel);
                }
            }
        }).show();
    }

    private void deleteUser(UserModel userModel) {
        mDatabase.child(userModel.getKey()).setValue(null);
        Toast.makeText(ViewUsers.this, "User deleted!", Toast.LENGTH_SHORT).show();
    }

    ProgressDialog progressDialog;
    public void showProgress(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
    }

    public void hideProgress(){
        if(progressDialog != null)
            progressDialog.hide();
    }
}
