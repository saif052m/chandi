package com.jewelery.chandi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jewelery.chandi.R;
import com.jewelery.chandi.model.UserModel;

import java.util.ArrayList;

public class UserAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<UserModel> mList;
    private LayoutInflater mLayoutInflater = null;

    public UserAdapter(Context context, ArrayList<UserModel> list) {
        mContext = context;
        mList = list;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return mList.size();
    }
    @Override
    public Object getItem(int pos) {
        return mList.get(pos);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        CompleteListViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(R.layout.user_item, null);
            viewHolder = new CompleteListViewHolder(v);
            v.setTag(viewHolder);
        }
        else {
            viewHolder = (CompleteListViewHolder) v.getTag();
        }

        UserModel model = mList.get(position);

        // set views data here
        viewHolder.uid.setText(model.getId());
        viewHolder.email.setText(model.getEmail());

        return v;
    }

    static class CompleteListViewHolder {
        // declare views here
        TextView uid, email;
        public CompleteListViewHolder(View base) {
            //initialize views here
            uid= (TextView) base.findViewById(R.id.uid);
            email= (TextView) base.findViewById(R.id.name);
        }
    }
}
