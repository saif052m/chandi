package com.jewelery.chandi.model;

public class RateModel {
    private String pattiRate, articleRate, pureRate, oldItemRate;

    public String getPattiRate() {
        if(pattiRate == null)
            pattiRate = "0";
        return pattiRate;
    }

    public void setPattiRate(String pattiRate) {
        this.pattiRate = pattiRate;
    }

    public String getArticleRate() {
        if(articleRate == null)
            articleRate = "0";
        return articleRate;
    }

    public void setArticleRate(String articleRate) {
        this.articleRate = articleRate;
    }

    public String getPureRate() {
        if(pureRate == null)
            pureRate = "0";
        return pureRate;
    }

    public void setPureRate(String pureRate) {
        this.pureRate = pureRate;
    }

    public String getOldItemRate() {
        if(oldItemRate == null)
            oldItemRate = "0";
        return oldItemRate;
    }

    public void setOldItemRate(String oldItemRate) {
        this.oldItemRate = oldItemRate;
    }
}
