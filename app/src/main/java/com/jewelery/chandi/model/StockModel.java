package com.jewelery.chandi.model;

public class StockModel {
    private String pattiStock, articleStock, pureStock;

    public String getPattiStock() {
        if(pattiStock == null)
            pattiStock = "0";
        return pattiStock;
    }

    public void setPattiStock(String pattiStock) {
        this.pattiStock = pattiStock;
    }

    public String getArticleStock() {
        if(articleStock == null)
            articleStock = "0";
        return articleStock;
    }

    public void setArticleStock(String articleStock) {
        this.articleStock = articleStock;
    }

    public String getPureStock() {
        if(pureStock == null)
            pureStock = "0";
        return pureStock;
    }

    public void setPureStock(String pureStock) {
        this.pureStock = pureStock;
    }
}
