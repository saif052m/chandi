package com.jewelery.chandi.model;

public class TransactionModel {
    private String date;
    private String patti;
    private String article;
    private String pure;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPatti() {
        return patti;
    }

    public void setPatti(String patti) {
        this.patti = patti;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public String getPure() {
        return pure;
    }

    public void setPure(String pure) {
        this.pure = pure;
    }
}
